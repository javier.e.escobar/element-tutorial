# element-tutorial



## Getting started

Start installing globally the element-cli package to run local simulations and test your scripts before launching them on Flood.io.

```
npm install -g @flood/element-cli
```

After that, install the playwright dependencies that will be needed by element-cli. This script will download headless versions of chrome, safari and firefox. So, confirm the download.

```
npx playwright install
```

## Test the installation

Run the following script

```
element run taller.ts
```

After running, a tmp folder will be created and the console should show something like this:

```
Test completed after 1 iterations
+------------+------------+------------+------------+------------+
| Iteration  |   Passed   |   Failed   |  Skipped   | Unexecuted |
|------------|------------|------------|------------|------------|
|     1      |     2      |     _      |     _      |     _      |
+------------+------------+------------+------------+------------+
```
