import { step, TestSettings, By, beforeAll, afterAll } from '@flood/element'

export const settings: TestSettings = {
	waitUntil: 'visible',
}

export default () => {
	beforeAll(async browser => {
		await browser.wait('500ms')
	})

	afterAll(async browser => {
		await browser.wait('500ms')
	})

	step('Step 1 visiting Google', async browser => {
		await browser.visit('https://www.google.co.ve/')
		await browser.takeScreenshot()
	})

	// browser keyword can be shorthanded as anything that is descriptive to you.
	step('Step 2 find a click first "Suerte" link', async browser => {

		const luckyButton = await browser.findElement(By.css('center:nth-child(1) > .RNmpXc'));
		// const firstLink = await browser.findElement(By.partialVisibleText('suerte'))

		await luckyButton.click()
		await browser.wait('3000ms')
		await browser.takeScreenshot()
	})

	
}
